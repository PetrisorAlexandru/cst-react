import React from "react";


import ListingEditScreen from "../Assets/app/screens/ListingEditScreen";
import MessagesScreen from "./app/screens/MessagesScreen";
import CategoryPickerItem from "./app/components/CategoryPickerItem";
import AccountScreen from "./app/screens/AccountScreen";
import ListingDetailsScreen from "./app/screens/ListingDetailsScreen";
import ListingScreen from "./app/screens/ListingScreen";
import LoginScreen from "./app/screens/LoginScreen";
import ViewImageScreen from "./app/screens/ViewImageScreen";
import WelcomeScreen from "./app/screens/WelcomeScreen";
import RegisterScreen from "./app/screens/RegisterScreen";
import ForgotPassword from "./app/screens/ForgotPassword";

export default function App() {
    return (
        <WelcomeScreen></WelcomeScreen>
    //   <RegisterScreen></RegisterScreen>
    // <ForgotPassword></ForgotPassword>
        // <LoginScreen></LoginScreen>
        // <AccountScreen></AccountScreen>
        // <ListingScreen></ListingScreen>
        // <ListingDetailsScreen></ListingDetailsScreen>
        // <ViewImageScreen></ViewImageScreen>
        // <ListingEditScreen></ListingEditScreen>
        // <MessagesScreen></MessagesScreen>
    );
}
