import React from "react";
import { View, Image, StyleSheet } from "react-native";
import AppText from "../components/AppText";
import colors from "../config/colors";
import ListItem from "../components/ListItem";

function ListingDetailsScreen() {
  return (
    <View>
      <Image style={styles.image} source={require("../assets/chair.jpg")} />
      <View style={styles.detailsContainer}>
        <AppText style={styles.title}>
          Scaun bucatarie / living fix Alberta, tapitat, lemn gri + stofa verde
        </AppText>
        <AppText style={styles.price}>229.00 RON</AppText>
        <View style={styles.listContainer}>
          <ListItem
            image={require("../assets/mosh.jpg")}
            title="Tandara Petrisor-Alexandru"
            subTitle="5 Listari"
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  detailsContainer: {
    padding: 20,
  },
  image: {
    width: "100%",
    height: 300,
  },
  price: {
    color: colors.green,
    fontWeight: "bold",
    fontSize: 20,
    marginVertical: 5,
  },
  title: {
    fontSize: 24,
    fontWeight: "500",
  },
  listContainer: {
      marginVertical: 25
  }
});

export default ListingDetailsScreen;
