import React from "react";
import { ImageBackground, StyleSheet, View, Image } from "react-native";
import AppButton from "../components/AppButton";


import colors from "../config/colors";

function WelcomeScreen() {
  return (
    <View style={styles.background}>
      <Image style={styles.logo} source={require("../assets/logo.png")} />
      <AppButton title="Acceseaza contul" />
      <AppButton title="Înregistrează-te" color="secondary" />
    </View>
  );
}

export default WelcomeScreen;

const styles = StyleSheet.create({
  background: {
    flex: 1,
    // backgroundColor: colors.background,
    justifyContent: "flex-end",
    alignItems: "center",
  },
  buttonsContainer: {
    padding: 20,
    width: '100%',
  },
  logo: {
    width: 250,
    height: 200,
    position: "absolute",
    top: 70,
  },
});
