import React from 'react';
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import Screen from '../components/Screen';
import { AppForm, AppFormField, SubmitButton } from "../components/forms";

const validationSchema = Yup.object().shape({
  name: Yup.string().required().label("Name"),
  email: Yup.string().required().email().label("Email"),
  password: Yup.string().required().min(4).label("Password"),
});

function ForgotPassword() {
    return (
      <Screen style={styles.container}>
        <AppForm
          initialValues={{ name: "", email: "", password: "" }}
          onSubmit={(values) => console.log(values)}
          validationSchema={validationSchema}
        >
          <AppFormField
            autoCorrect={false}
            icon="account"
            name="name"
            placeholder="Nume utilizator"
          />
          <AppFormField
            autoCapitalize="none"
            autoCorrect={false}
            icon="email"
            keyboardType="email-address"
            name="email"
            placeholder="Email"
            textContentType="emailAddress"
          />
          <SubmitButton title="Am uitat parola" />
        </AppForm>
      </Screen>
    );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default ForgotPassword;
