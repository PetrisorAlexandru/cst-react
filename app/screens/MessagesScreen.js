import React, { useState } from "react";
import { FlatList, StyleSheet, View } from "react-native";

import ListItem from "../components/ListItem";
import ListItemSeparator from "../components/ListItemSeparator";
import Screen from "../components/Screen";
import ListItemDeleteAction from "../components/ListItemDeleteAction";

const initialMessages = [
  {
    id: 1,
    title: "Andrei Cosmin",
    description: "Salutare! Sunt interesat de produsul ...",
    image: require("../assets/andrei.jpg"),
  },
  {
    id: 2,
    title: "Alexandra Maria",
    description: "Hei! Mi-ai putea oferi mai multe detalii ...",
    image: require("../assets/alexandra.jpg"),
  },
];

function MessagesScreen() {
  const [messages, setMessages] = useState(initialMessages);
  const [refreshing, setRefreshing] = useState(false);

  const handleDelete = (message) => {
    setMessages(messages.filter((m) => m.id !== message.id));
  };

  return (
    <Screen>
      <FlatList
        data={messages}
        keyExtractor={(message) => message.id.toString()}
        renderItem={({ item }) => (
          <ListItem
            title={item.title}
            subTitle={item.description}
            image={item.image}
            onPress={() => console.log("Mesaj selectat", item)}
            renderRightActions={() => (
              <ListItemDeleteAction onPress={() => handleDelete(item)} />
            )}
          ></ListItem>
        )}
        ItemSeparatorComponent={ListItemSeparator}
        refreshing={refreshing}
        onRefresh={() => {
          setMessages([
            {
              id: 1,
              title: "Andrei Cosmin",
              description: "Salutare! Sunt interesat de produsul ...",
              image: require("../assets/andrei.jpg"),
            },
            {
              id: 2,
              title: "Alexandra Maria",
              description: "Hei! Mi-ai putea oferi mai multe detalii ...",
              image: require("../assets/alexandra.jpg"),
            },
          ]);
        }}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({});

export default MessagesScreen;
