import React from "react";
import { FlatList, StyleSheet, View } from "react-native";
import Card from "../components/Card";

import Screen from "../components/Screen";
import colors from "../config/colors";

const listing = [
  {
    id: 1,
    title:
      "Scaun bucatarie / living fix Alberta, tapitat, lemn gri + stofa verde",
    price: "229.00",
    image: require("../assets/chair.jpg"),
  },
  {
    id: 2,
    title:
      "Canapea extensibila 3 locuri lanis, cu lada, maro + roz, 190 x 92 x 86 cm, 1C",
    price: "1229.00",
    image: require("../assets/couch.jpg"),
  },
];

function ListingScreen() {
  return (
    <Screen style={styles.screen}>
      <FlatList
        data={listing}
        keyExtractor={(listing) => listing.id.toString()}
        renderItem={({ item }) => (
          <Card
            title={item.title}
            subTitle={item.price + " RON"}
            image={item.image}
          />
        )}
      />
    </Screen>
  );
}

const styles = StyleSheet.create({
    screen: {
        padding: 15,
        backgroundColor: colors.lightGray
    }
})

export default ListingScreen;
