import React from "react";
import { StyleSheet } from "react-native";
import * as Yup from "yup";

import Screen from "../components/Screen";
import {
  AppForm,
  AppFormField,
  AppFormPicker,
  SubmitButton,
} from "../components/forms";
import CategoryPickerItem from "../components/CategoryPickerItem";

const validationSchema = Yup.object().shape({
  title: Yup.string().required().min(1).label("Titlu"),
  price: Yup.number().required().min(1).max(100000).label("Preț produs"),
  description: Yup.string().label("Descriere produs"),
  category: Yup.object().required().nullable().label("Categorie produs"),
});

const categories = [
  {
    value: 1,
    label: "Articole sportive",
    backgroundColor: "#45aaf2",
    icon: "basketball",
  },
  {
    value: 2,
    label: "Cărți",
    backgroundColor: "#a55eea",
    icon: "book-open-variant",
  },
  {
    value: 3,
    label: "Diverse",
    backgroundColor: "#778ca3",
    icon: "application",
  },
  {
    value: 4,
    label: "Electrocasnice",
    backgroundColor: "#fed330",
    icon: "camera",
  },
  {
    value: 5,
    label: "Îmbrăcăminte",
    backgroundColor: "#2bcbba",
    icon: "shoe-heel",
  },
  {
    value: 6,
    label: "Jocuri video",
    backgroundColor: "#26de81",
    icon: "cards",
  },
  {
    value: 7,
    label: "Mobilă",
    backgroundColor: "#fc5c65",
    icon: "floor-lamp",
  },
  {
    value: 8,
    label: "Muzică",
    backgroundColor: "#4b7bec",
    icon: "headphones",
  },
  {
    value: 9,
    label: "Vehicule auto",
    backgroundColor: "#fd9644",
    icon: "car",
  },
];

function ListingEditScreen() {
  return (
    <Screen style={styles.container}>
      <AppForm
        initialValues={{
          title: "",
          price: "",
          description: "",
          category: null,
        }}
        onSubmit={(values) => console.log(values)}
        validationSchema={validationSchema}
      >
        <AppFormField maxLength={50} name="title" placeholder="Titlu produs" />
        <AppFormField
          keyboardType="numeric"
          maxLength={6}
          name="price"
          placeholder="Preț produs"
          width={150}
        />
        <AppFormPicker
          items={categories}
          name="category"
          PickerItemComponent={CategoryPickerItem}
          numberOfColumns={3}
          placeholder="Titlu produs"
          width="50%"
        />
        <AppFormField
          maxLength={255}
          multiline
          name="description"
          numberOfLines={3}
          placeholder="Descriere produs"
        />
        <SubmitButton title="Posteaza anunț" />
      </AppForm>
    </Screen>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
});

export default ListingEditScreen;
