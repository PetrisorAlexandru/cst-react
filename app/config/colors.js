export default {
  primary: "#185ADB",
  secondary: "#0A1931",
  accent: "#FFC947",
  background: "#EFEFEF",
  peach: "#FEDDBE",
  green: "#66DE93",
  black: "#000",
  white: "#fff",
  gray: "#6E6969",
  lightGray: "#E8E8E8",
  danger: "#ff5252"
};
