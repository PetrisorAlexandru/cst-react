import React from 'react';
import { View, TextInput, StyleSheet } from 'react-native';
import {MaterialCommunityIcons} from "@expo/vector-icons"
import colors from '../config/colors';

function AppTextInput({icon, placeHolder, width = "100%", ...otherProps}) {
    return (
      <View style={[styles.container, {width}]}>
        {icon && <MaterialCommunityIcons name={icon} size={30} color={colors.secondary} style={styles.icon}/>}
        <TextInput
        placeholderTextColor={colors.gray}
        style={styles.textInput} placeholder={placeHolder} {...otherProps}/>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.lightGray,
    borderRadius: 25,
    flexDirection: "row",
    padding: 15,
    marginVertical: 10,
  },
  textInput: {
    fontSize: 18,
    color: colors.secondary,
    fontFamily: Platform.OS === "android" ? "Roboto" : "Avenir",
  },
  icon: {
      marginRight: 10
  }
});

export default AppTextInput;
